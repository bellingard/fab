var Person = function(first, last, middle) {
    this.first = first;
    this.middle = middle;
    this.last = last;
};

Person.prototype = {

    whoAreYou : function() {
        var myNumber = 010;   // myNumber will hold 8, not 10 - was this really expected?
        return this.first + (this.middle ? ' ' + this.middle: '') + ' ' + this.last + myNumber;
    }

};
